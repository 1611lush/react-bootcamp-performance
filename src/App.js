import React from "react";
import { memo, useCallback, useEffect, useState } from "react";
import "./App.css";


const Button = memo((props) => {
	console.count("render button");
	return <button {...props} style={{ backgroundColor: "lightgray" }} />
})

const ListItem = memo(({ children }) => {
	console.count("render list item")
		return (
			<li>
			{children}
			<label>
				<input type="checkbox" />
				Add to cart
			</label>
		</li>  
	)
})



const App = memo(() => {
	const [searchString, setSearchString] = useState("");
	const [isSortingDesc, setSortingDesc] = useState(false);
	const [products, setProducts] = useState([]);

	useEffect(() => {
		console.count("render fetch");
		fetch("https://reqres.in/api/products")
			.then((response) => response.json())
			.then((json) =>
				setProducts(
					json.data
						.filter((item) => item.name.includes(searchString))
						.sort((a, z) =>
							isSortingDesc
								? z.name.localeCompare(a.name)
								: a.name.localeCompare(z.name)
						)
				)
			);
	}, [searchString, isSortingDesc]);

	console.count("render app");


	const searchResult = useCallback(
		(e) => setSearchString(e.target.value), []
	);
	const sortResult = useCallback(() => {
		setSortingDesc((value) => !value)
	}, []);



	return (
		<div className="App">
			<input
				type="search"
				value={searchString}
				onChange={searchResult}
			/>
			<Button onClick={sortResult}>
				Change sort direction
			</Button>
			<ul>
				{products.map((product) => (
					<ListItem key={product.id}>{product.name}</ListItem>
				))}
			</ul>
		</div>
	);
})


export default App;
